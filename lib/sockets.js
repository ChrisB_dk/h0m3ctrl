#!/usr/bin/env node
'use strict'

/**
  *   the things that the robot can do
  */

/* get the socket.io module */
const io = require( 'socket.io' )()

/* export our sockets */
module.exports = {
	init: ( server ) => {
		io.attach( server )
		io.on( 'connection', ( socket ) => {
			socket.emit( 'sutats' )
			socket.on( 'status', ( data ) => {
		  	console.log( 'Status: ' + data.ready )
			})
		})
	},
	sock: io
}
