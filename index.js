#!/usr/bin/env node
'use strict'

/**
  * H0m3 control
  *
  *   from the desktop, bed and / or "not in the face"
  *   Bjørn 'Im still not dancing jive' Christiansen : ChrisB.dk'
  *
  */

// get stuff
const wire = require( 'wiring-pi' )

const serving = require( './lib/serving' )
const websock = require( './lib/sockets' )
const config = require( './config' )


var value = 1
var pin = 108

wire.setup( 'wpi' )
wire.mcp23s17Setup( 100, 0, 0 )
wire.pinMode( pin, OUTPUT )

setInterval( function () {
  wire.digitalWrite( pin, value )
  value = +!value
}, 500 )

// activate the stuff
serving.html()
websock.init( serving.serv )
